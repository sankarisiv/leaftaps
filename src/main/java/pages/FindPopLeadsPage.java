package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import wdMethods.ProjectMethods;

public class FindPopLeadsPage extends ProjectMethods {
	
	public FindPopLeadsPage() {
		PageFactory.initElements(driver, this);
	}
	@FindBy(xpath="//input[@name='firstName']")
	WebElement eleFirstName;
	public FindPopLeadsPage TypeFirstName(String data)
	{
		
		type(eleFirstName, data);
		return this;
		
	}
	
	@FindBy(xpath="//button[text()='Find Leads']")
	WebElement eleClickFindLeads;
	public FindPopLeadsPage clickFindLeads() {
		
		click(eleClickFindLeads);
		return this;
		
	}

		
		
		
		
		
	}

