package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class ViewLeadPage extends ProjectMethods{



	public ViewLeadPage verifyFirstName(String data) {

		WebElement eleFirstName = locateElement("id","viewLead_firstName_sp");
		verifyExactText(eleFirstName, data);
		System.out.println(eleFirstName);
		return this;

	}


	public ViewLeadPage verifyViewLeadPage() {

		verifyTitle(getTitle());
		System.out.println(getTitle());
		return this;

	}
}







