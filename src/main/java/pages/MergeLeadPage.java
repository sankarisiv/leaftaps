package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MergeLeadPage extends ProjectMethods  {
	

	public MyMergePage clickMergeLead  () {
		WebElement eleMergeLeads = locateElement("link","Merge Leads");
		click(eleMergeLeads);
		return new MyMergePage();

}
}
