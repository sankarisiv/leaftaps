package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class HomePage extends ProjectMethods{

	
	
	public LoginPage clickLogOut() {
		WebElement eleLogOut = locateElement("class", "decorativeSubmit");
	    click(eleLogOut);  
	    return new LoginPage();
	}
	
	public MyHomePage clickCRM() {
		WebElement eleCRM = locateElement("link","CRM/SFA");
		click(eleCRM);
		return new MyHomePage();
		
	}
	
}







