package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MyMergePage extends ProjectMethods {
	public FindPopLeadsPage clickFromLead  () {
		WebElement eleFromLeads = locateElement("xpath","//img[@src='/images/fieldlookup.gif'][1]");
		click(eleFromLeads);
		switchToWindow(1);
		return new FindPopLeadsPage();
				}
}
