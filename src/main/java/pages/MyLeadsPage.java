package pages;

import org.openqa.selenium.WebElement;

import wdMethods.ProjectMethods;

public class MyLeadsPage extends ProjectMethods{

	
	
	public CreateLeadPage clickCreateLeadLink() {
		WebElement eleCreateLead = locateElement("link","Create Lead");
		click(eleCreateLead);
		return new CreateLeadPage();
		
	}
	
}







