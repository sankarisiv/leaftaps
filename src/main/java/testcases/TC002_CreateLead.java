package testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
import wdMethods.ProjectMethods;

public class TC002_CreateLead extends ProjectMethods{
	@BeforeTest
	public void setData() {
		testCaseName = "TC001_LogInLogout";
		testDescription = "Login into leaftaps";
		testNodes = "Leads";
		authors ="Gayatri";
		category="smoke";
		dataSheetName = "TC002";
	}

	@Test(dataProvider="fetchData")
	public void CreateLead(String uname,String pwd,String cname, String fname, String lname, String title) {
		new LoginPage()
		.typeUsername(uname)
		.typePassword(pwd)
		.clickLogin().clickCRM().clickLeads().clickCreateLeadLink()
		.typeCompanyName(cname).typeFirstName(fname).typeLastName(lname).clickCreateLead().verifyFirstName(fname).verifyTitle(title);

	}
}
